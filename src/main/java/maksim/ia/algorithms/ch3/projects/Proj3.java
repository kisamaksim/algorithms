package maksim.ia.algorithms.ch3.projects;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class Proj3 {
    
    public static void main(String[] args) {
        int[] arr = new int[] {67, 99, 78, 99, 0, 99, 4, 77, 4, 90, 178, 696, 1, 77, 768, 4};
        System.out.println(Arrays.toString(arr));
        insertSort(arr);
        System.out.println(Arrays.toString(arr));
        noDups(arr);
        System.out.println(Arrays.toString(arr));
    }
    
    public static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int marker = i;
            while (marker > 0 && array[marker - 1] > tmp) {
                array[marker] = array[marker - 1];
                marker--;
            }
            if (marker != i) {
                array[marker] = tmp;
            }
        }
    }
    
    public static void noDups(int[] array) {
        int[] offsets = new int[array.length];
        int duplicateCount = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                duplicateCount++;
                int tmp = array[i];
                array[i] = 0;
                for (int j = i + 1; j < array.length; j++) {
                    if (tmp == array[j]){
                        duplicateCount++;
                        array[j] = 0;
                    } else {
                        i = j;
                        break;
                    }
                }
            }
            offsets[i] = duplicateCount;
        }
    
        for (int i = 0; i < array.length; i++) {
            int tmp = array[i];
            array[i] = 0;
            array[i - offsets[i]] = tmp;
        }
    }
}

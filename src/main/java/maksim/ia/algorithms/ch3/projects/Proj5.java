package maksim.ia.algorithms.ch3.projects;

import java.util.Arrays;

public class Proj5 {
    
    public static void main(String[] args) {
        int[] array = {67, 78, 99, 0, 4, 77, 90, 178, 696, 1};
        System.out.println(Arrays.toString(array));
        insertionSort(array);
        System.out.println(Arrays.toString(array));
    
        System.out.println("----------");
        int[] arrayOne = {13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        System.out.println(Arrays.toString(arrayOne));
        insertionSort(arrayOne);
        System.out.println(Arrays.toString(arrayOne));
    
        System.out.println("----------");
        int[] arrayTwo = {1, 2, 3, 4, 7, 6, 5, 8, 9, 10};
        System.out.println(Arrays.toString(arrayTwo));
        insertionSort(arrayTwo);
        System.out.println(Arrays.toString(arrayTwo));
    }
    
    public static void insertionSort(int[] array) {
        int copyCount = 0;
        int compareCount = 0;
        for (int out = 1; out < array.length; out++) {
            int tmp = array[out];
            int marker = out;
            while (true) {
                if (marker > 0) {
                    compareCount++;
                    if (array[marker - 1] > tmp) {
                        array[marker] = array[marker - 1];
                        copyCount++;
                        marker--;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            if (marker != out) {
                array[marker] = tmp;
            }
        }
        System.out.println("copy count: " + copyCount);
        System.out.println("compare count: " + compareCount);
        
    }
}

package maksim.ia.algorithms.ch3.projects;

import java.util.Arrays;

public class Proj4 {
    
    public static void main(String[] args) {
        int[] arr = new int[] {67, 99, 78, 99, 0, 99, 4, 77, 4, 90, 178, 696, 1, 77, 768, 4};
        System.out.println(Arrays.toString(arr));
        oddEvenSort(arr);
        System.out.println(Arrays.toString(arr));
    
        int[] arrOne = new int[] {67, 99, 78, 99, 0, 99, 4, 77, 4, 90, 178, 696, 1, 77, 768, 4};
        System.out.println(Arrays.toString(arrOne));
        bubbleSort(arrOne);
        System.out.println(Arrays.toString(arrOne));
    }
    
    public static void bubbleSort(int[] array) {
        for (int out = array.length - 1; out >= 1 ; out--) {
            for (int in = 0; in < out; in++) {
                if (array[in] > array[in + 1]) {
                    int tmp = array[in];
                    array[in] = array[in +1];
                    array[in + 1] = tmp;
                }
            }
        }
    }
    
    public static void oddEvenSort(int[] array) {
        int exit = 1;
        int count = 0;
        while (exit != 0) {
            exit = 0;
            count++;
            for (int i = 1; i < array.length - 1; i+=2) {
                if (array[i] > array[i + 1]) {
                    int tmp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = tmp;
                    exit++;
                }
            }
            
            for (int i = 0; i < array.length - 1; i+=2) {
                if (array[i] > array[i + 1]) {
                    int tmp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = tmp;
                    exit++;
                }
            }
        }
        System.out.println("count: " + count);
    }
}

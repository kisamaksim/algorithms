package maksim.ia.algorithms.ch3.projects;

import java.util.Arrays;

public class Proj6 {
    
    public static void main(String[] args) {
        int[] dupsArray = {1,1,3,4,5,6,6,7,1,8,9,7,7,5,6};
    
        System.out.println(Arrays.toString(dupsArray));
        insertionSortRemoveDups(dupsArray);
        System.out.println(Arrays.toString(dupsArray));
        insertSort(dupsArray);
        System.out.println(Arrays.toString(dupsArray));
        cleanArray(dupsArray);
        System.out.println(Arrays.toString(dupsArray));
    }
    
    public static void insertionSortRemoveDups(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int marker = i;
            while (marker > 0 && (array[marker - 1] >= tmp || array[marker - 1] == -1)) {
                if (array[marker - 1] == tmp) {
                    array[marker - 1] = -1;
                    break;
                }
                array[marker] = array[marker - 1];
                marker--;
            }
            if (marker != i) {
                array[marker] = tmp;
            }
            
        }
    }
    
    public static void insertSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int tmp = array[i];
            int marker = i;
            while(marker > 0 && array[marker - 1] > tmp) {
                array[marker] = array[marker - 1];
                marker--;
            }
            if (marker != i) {
                array[marker] = tmp;
            }
        }
    }
    
    public static void cleanArray(int[] array) {
        int count = 0;
        for (int value : array) {
            if (value == -1) {
                count++;
            } else {
                break;
            }
        }
        System.out.println(Arrays.toString(Arrays.copyOfRange(array, count, array.length - 1)));
    }
}

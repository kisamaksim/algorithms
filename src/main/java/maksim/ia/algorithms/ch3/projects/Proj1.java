package maksim.ia.algorithms.ch3.projects;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Proj1 {
    
    public static void main(String[] args) {
        int[] arr = new int[] {67, 78, 99, 0, 4, 77, 90, 178, 696, 1};
        System.out.println(Arrays.toString(arr));
        bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
        
        int[] arrOne = new int[25];
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 0; i < arrOne.length; i++) {
            arrOne[i] = random.nextInt(1000);
        }
        System.out.println(Arrays.toString(arrOne));
        bubbleSort(arrOne);
        System.out.println(Arrays.toString(arrOne));
        
    }
    
    public static void bubbleSort(int[] array) {
        for (int outR = array.length - 1, outL = 0; outR >= 1; outR--, outL++) {
            for (int inR = outL; inR < outR; inR++) {
                if (array[inR] > array[inR + 1]) {
                    int tmp = array[inR];
                    array[inR] = array[inR + 1];
                    array[inR + 1] = tmp;
                }
            }
            
            for (int inL = outR - 1; inL > outL ; inL--) {
                if (array[inL] < array[inL - 1]) {
                    int tmp = array[inL];
                    array[inL] = array[inL - 1];
                    array[inL - 1] = tmp;
                }
            }
        }
    }
}

package maksim.ia.algorithms.ch3.projects;

import java.util.Arrays;

public class Proj2 {
    
    public static void main(String[] args) {
        int[] arr = new int[] {67, 78, 99, 0, 4, 77, 90, 178, 696, 1, 8};
        System.out.println(Arrays.toString(arr));
        System.out.println(median(arr));
    }
    
    // никак не обрабатываю ситуацию, когда в центре оказываются два числа, например 41 и 42
    public static int median(int[] array) {
        int[] copied = Arrays.copyOf(array, array.length);
        insertSort(copied);
        System.out.println(Arrays.toString(copied));
        int middleIndex = copied.length / 2;
        int middleR = copied[middleIndex];
        int middleL = copied[middleIndex - 1];
        
        if (middleR - 1 == middleL) {
            return -1;
        } else {
            return middleR - 1;
        }
    }
    
    public static void insertSort(int[] array) {
        int tmp, in;
        for (int i = 1; i < array.length; i++) {
            tmp = array[i];
            in = i;
            while (in > 0 && array[in - 1] >= tmp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = tmp;
        }
    }
}

package maksim.ia.algorithms.ch3;

import java.util.Arrays;

public class CommonMain {
    public static void main(String[] args) {
        //bubble
        int[] array = {67, 78, 99, 0, 4, 77, 90, 178, 696, 1};
        
        bubbleSort(array);
        System.out.println("bubble " + Arrays.toString(array));
        
        //select
        int[] select = {67, 78, 99, 0, 4, 77, 90, 178, 696, 1};
    
        selectSort(select);
        System.out.println("select " + Arrays.toString(select));
    
        //insert
        int[] insert = {67, 78, 99, 0, 4, 77, 90, 178, 696, 1};
    
        insertSort(insert);
        System.out.println("insert " + Arrays.toString(insert));
    
        //insert book
        int[] insertBook = {67, 78, 99, 0, 4, 77, 90, 178, 696, 1};
    
        insertSortFromBook(insertBook);
        System.out.println("insertBook " + Arrays.toString(insertBook));
    
    }
    
    public static void bubbleSort(int[] array) {
        for (int out = array.length - 1; out >= 1 ; out--) {
            for (int in = 0; in < out; in++) {
                if (array[in] > array[in + 1]) {
                    int g = array[in];
                    array[in] = array[in + 1];
                    array[in + 1] = g;
                }
            }
        }
    }
    
    public static void selectSort(int[] array) {
        int minIndex;
        for (int i = 0; i < array.length - 1; i++) {
            minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[minIndex] > array[j]) {
                    minIndex = j;
                }
            }
            if (minIndex == i) {
                continue;
            }
            int q = array[i];
            array[i] = array[minIndex];
            array[minIndex] = q;
        }
    }
    
    public static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int marker = array[i];
            int indexForMarker = i;
            for (int j = i - 1; j >= 0; j--) {
                if (array[j] > marker) {
                    array[j + 1] = array[j];
                    indexForMarker = j;
                } else {
                    indexForMarker = j + 1;
                    break;
                }
            }
            if (indexForMarker != i) {
                array[indexForMarker] = marker;
            }
        }
    }
    
    public static void insertSortFromBook(int[] array) {
        int tmp, in;
        for (int i = 1; i < array.length; i++) {
            tmp = array[i];
            in = i;
            while (in > 0 && array[in - 1] >= tmp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = tmp;
        }
    }
}

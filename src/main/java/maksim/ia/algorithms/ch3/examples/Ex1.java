package maksim.ia.algorithms.ch3.examples;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import maksim.ia.algorithms.ch3.CommonMain;

public class Ex1 {
    
    private static ThreadLocalRandom random = ThreadLocalRandom.current();
    
    public static void main(String[] args) {
        int[] array = new int[120_000];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(999);
        }
        System.out.println(Arrays.toString(array));
        long start = System.currentTimeMillis();
        CommonMain.bubbleSort(array); // 30 sec on 120k
//        CommonMain.selectSort(array); // 8 sec on 120k
//        CommonMain.insertSort(array); // 2 sec on 120k
        long end = System.currentTimeMillis();
        System.out.println("time in sec: " + TimeUnit.MILLISECONDS.toSeconds(end - start));
        System.out.println(Arrays.toString(array));
        
    }
}

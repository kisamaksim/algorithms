package maksim.ia.algorithms.ch2;

public class MainBinarySearch {
    public static void main(String[] args) {
        int[] array = {1,2,5,6,78,79,88,99,167,456,567,789};
    
        System.out.println(binSearch(array, 1));
        System.out.println(binSearch(array, 78));
        System.out.println(binSearch(array, 456));
        System.out.println(binSearch(array, 789));
        
        System.out.println(binSearch(array, -19));
        System.out.println(binSearch(array, 900));
    }
    
    private static int binSearch(int[] array, int value) {
        int h = array.length - 1;
        int l = 0;
        
        int middle;
        
        while (l <= h) {
            middle = (h + l) / 2;
            
            if (array[middle] == value) {
                return middle;
            }
            
            if (array[middle] > value) {
                h = middle - 1;
            } else {
                l = middle + 1;
            }
        }
        return -1;
    }
}

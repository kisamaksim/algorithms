package maksim.ia.algorithms.ch2;

import java.util.Arrays;

public class MainTwoPointSix {
    public static void main(String[] args) {
        int[] dupsArray = {1,1,3,4,5,6,6,7,1,8,9,7,7,5,6};
        
        System.out.println(Arrays.toString(dupsArray));
        noDups(dupsArray);
        System.out.println(Arrays.toString(dupsArray));
    
    }
    
    public static void noDups(int[] array) {
        
        for (int i = 0; i < array.length; i++) {
            if (array[i] == -1) {
                continue;
            }
            for (int j = 0; j < array.length; j++) {
                if (!(j == i)) {
                    if (array[i] == array[j]) {
                        array[j] = -1;
                    }
                }
            }
        }
    
        for (int i = 0; i < array.length; i++) {
            if (array[i] == - 1) {
                for (int j = i; j < array.length - 1; j++) {
                    array[j] = array[j + 1];
                }
                i--;
                array[array.length - 1] = 0;
            }
        }
    }
}

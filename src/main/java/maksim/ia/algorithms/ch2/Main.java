package maksim.ia.algorithms.ch2;

import java.util.Arrays;

public class Main {
    
    public static void main(String[] args) {
        HighArray highArray = new HighArray(10);
        highArray.insert(45);
        highArray.insert(56);
        highArray.insert(78);
        highArray.insert(99);
        highArray.insert(1098);
        highArray.insert(5);
        highArray.insert(1);
        highArray.insert(4587);
        highArray.insert(8);
        
        System.out.println(highArray.getMax());
    
        HighArray empty = new HighArray(190);
    
        System.out.println(empty.getMax());
        
        //---- removeMax
        
//        System.out.println(Arrays.toString(highArray.getArr()));
//        System.out.println(highArray.removeMax());
//        System.out.println(Arrays.toString(highArray.getArr()));
        
        //---- removeMax as sorting
        
        int[] newArr = new int[10];
        for (int i = 0; i < highArray.getArr().length; i++) {
            newArr[i] = highArray.removeMax();
        }
        System.out.println(Arrays.toString(newArr));
    
        System.out.println("-----------------------------");
    
        OrderedArray orderedArray = new OrderedArray(14);
        orderedArray.simpleAdd(1);
        orderedArray.simpleAdd(2);
        orderedArray.simpleAdd(3);
        orderedArray.simpleAdd(4);
        orderedArray.simpleAdd(5);
        orderedArray.simpleAdd(6);
        orderedArray.simpleAdd(7);
        orderedArray.simpleAdd(8);
        orderedArray.simpleAdd(9);
        orderedArray.simpleAdd(10);
    
        System.out.println(orderedArray.binaryFind(9));
        System.out.println(orderedArray.binaryFind(10));
        System.out.println(orderedArray.binaryFind(1));
        
        System.out.println(orderedArray.binaryFind(-1));
        System.out.println(orderedArray.binaryFind(198));
        
//        orderedArray.binaryInsert(150);
    
        System.out.println("-----------------------------");
    
        OrderedArray orderedArray1 = new OrderedArray(16);
        orderedArray1.binaryInsert(56);
        orderedArray1.binaryInsert(55);
        orderedArray1.binaryInsert(0);
        orderedArray1.binaryInsert(59);
        System.out.println();
    
    }
    
    private static class HighArray {
        private int[] arr;
        private int nElem;
        
        public HighArray(int size) {
            arr = new int[size];
            nElem = 0;
        }
        
        public int[] getArr() {
            return arr;
        }
        
        public void insert(int value) {
            arr[nElem] = value;
            nElem++;
        }
        
        public int getMax() {
            if (nElem == 0) {
                return -1;
            }
            int max = 0;
            for (int i = 0; i < nElem; i++) {
                if (arr[i] > max) {
                    max = arr[i];
                }
            }
            return max;
        }
        
        public int removeMax() {
            if (nElem == 0) {
                return -1;
            }
            int max = 0;
            int maxIndex = 0;
            for (int i = 0; i < nElem; i++) {
                if (arr[i] > max) {
                    max = arr[i];
                    maxIndex = i;
                }
            }
            for (int i = maxIndex; i < nElem; i++) {
                arr[i] = arr[i + 1];
            }
            return max;
        }
    }
    
    private static class OrderedArray {
        private int[] array;
        private int nElem;
        
        public OrderedArray(int size) {
            array = new int[size];
            nElem = 0;
        }
        
        public void simpleAdd(int value) {
            array[nElem] = value;
            nElem++;
        }
        
        public int binaryInsert(int value) {
            if (nElem == array.length) {
                return -1;
            }
            if (nElem == 0) {
                array[nElem] = value;
                return nElem++;
            }
            
            int h = nElem;
            int l = 0;
            int middle = 0;
            
            while (l < h) {
                middle = (h + l) / 2;
                if (array[middle] > value) {
                    h = middle - 1;
                } else {
                    l = middle + 1;
                }
            }
            
            int index = 0;
            if (array[middle] > value) {
                if (middle != 0 && array[middle - 1] > value) {
                    index = moveUp(middle - 1);
                } else {
                    index = moveUp(middle);
                }
            } else {
                if (nElem != middle + 1 && array[middle + 1] < value) {
                    index = moveUp(middle + 2);
                } else {
                    index = moveUp(middle + 1);
                }
            }
            array[index] = value;
            nElem++;
            return index;
        }
        
        public void binaryRemove(int value) {
            int index = binaryFind(value);
            if (index != -1) {
                moveDown(index);
                nElem--;
            }
        }
        
        private int moveUp(int moveIndex) {
            for (int i = nElem; i > moveIndex; i--) {
                array[i] = array[i - 1];
            }
            return moveIndex;
        }
    
        private int moveDown(int moveIndex) {
            for (int i = moveIndex; i < nElem; i++) {
                array[i] = array[i + 1];
            }
            return moveIndex;
        }
        
        public int binaryFind(int value) {
            int h = nElem;
            int l = 0;
            
            int middle;
            
            while (l <= h) {
                middle = (h + l) / 2;
                
                if (array[middle] == value) {
                    return middle;
                }
                
                if (array[middle] > value) {
                    h = middle - 1;
                } else {
                    l = middle + 1;
                }
            }
            return -1;
        }
    }
}

package maksim.ia.algorithms.ch2;

import java.util.Arrays;

public class MainTwo {
    
    public static void main(String[] args) {
        //without copy
        int[] first = {1, 2, 34, 87, 88, 98, 178, 567, 879, 980};
        int[] second = {3, 4, 33, 35, 44, 678};
    
        int[] merge = merge(first, second);
        System.out.println(Arrays.toString(merge));
    
        // with copy
        int[] firstC = {1, 2, 34, 87, 88, 98, 178, 567, 879, 980};
        int[] secondC = {3, 4, 34, 35, 87, 678};
    
        int[] mergeC = merge(firstC, secondC);
        System.out.println(Arrays.toString(mergeC));
    }
    
    public static int[] merge(int[] first, int[] second) {
        int[] result = new int[first.length + second.length];
        
        int f = 0;
        int s = 0;
        int fLength = first.length;
        int sLength = second.length;
        
        for (int i = 0 ; i < result.length ; i++) {
            if (fLength == 0 || sLength == 0) {
                if (fLength == 0) {
                    result[i] = second[s];
                    s++;
                } else {
                    result[i] = first[f];
                    f++;
                }
            } else {
                if (first[f] < second[s]) {
                    result[i] = first[f];
                    f++;
                    fLength--;
                } else {
                    result[i] = second[s];
                    s++;
                    sLength--;
                }
            }
        }
        
        return result;
    }
}
